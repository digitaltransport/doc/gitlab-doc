module.exports = {
    title: 'GitLab Documentation',
    description: 'Documentation for the Digital Transport Resource Center',
    base: '/gitlab-doc/',
    dest: 'public',
    themeConfig: {
        sidebar: [
          {
            collapsable: false,
            children: [
              '/data-faq/',
            ]
          },
          {
            collapsable: false,
            children: [
              '/doc-faq/',
            ]
          }
        ],
        activeHeaderLinks: true,
        displayAllHeaders: true,
        sidebarDepth: 2,
        repo: 'https://git.digitaltransport4africa.org/doc/gitlab-doc',
        docsDir: '.',
        editLinks: true,
        editLinkText: 'Edit on GitLab',
        nav: [
            {text: 'DT4A', link: 'https://digitaltransport4africa.org/'},
            {text: 'Resource Center', link: 'https://git.digitaltransport4africa.org/'},
        ],
        lastUpdated: 'Last update',
	}
}
