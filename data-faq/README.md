# Data FAQ

Here are some of the most Frequently Asked Questions about data on [Digital Transport for Africa](http://digitaltransport4africa.org/) (DT4A). Feel free to [ask new questions](https://git.digitaltransport4africa.org/doc/gitlab-doc/issues/new)!

## What's data?

**Data are just files on your computer**: documents, spreadsheets, images... A dataset is a collection of data, sometimes multiple files in a compressed ZIP file (`.zip`), sometimes multiple sheets in an Excel file or even a single CSV file (`.csv`) with multiple columns.

For example, [GTFS](https://gtfs.org/) (also known as GTFS-Static, not to be confused with GTFS-RT) is a common format to share public transit data. It's a ZIP file (`GTFS.zip`) containing text files (`.txt`) with [comma-separated values](https://en.wikipedia.org/wiki/Comma-separated_values).

## What's open data?

Open data is data that is shared in a way that others have the rights to use and republish it. You give these rights to people by sharing your data with an open data license (e.g. [CC0](https://creativecommons.org/publicdomain/zero/1.0/) or [ODbL](https://opendatacommons.org/licenses/odbl/summary/index.html)). Without a license, nobody knows if they can use this data, therefore it is not open.

## What's Gitlab?

The DT4A project provides a platform to upload, publish and share data, accessible here: [git.digitaltransport4africa.org](http://git.digitaltransport4africa.org/). It's an instance of the open source tool called Gitlab. Gitlab is the open source equivalent of [Github](http://github.com/). DT4A's Gitlab is not to be confused with [Gitlab.com](http://about.gitlab.com/). Both are independent and separate websites.

For the sake of simplicity, we call "Gitlab" or "Resource Center" the website at **git.digitaltransport4africa.org**. It looks like this:

![](../assets/gitlab_home_page.png)

## How to register on Gitlab?

In order to have access to most features on Gitlab, such as uploading data, you will need to register a personal account. To do so, [click here](https://git.digitaltransport4africa.org/users/sign_in?redirect_to_referer=yes#register-pane) and complete the Register form:

![](../assets/signup_page.png)

You can also directly sign in with a user account on Gitlab.com or Github.com, if you already have one there, at the bottom of the page.

:warning: Some email providers, which tend to be used by spammers, are banned or configured to have limited functionality on the Gitlab Resource Center. Please use your professional account or another email provider to register. [Contact the administrators](mailto:contact@jailbreak.paris) to request a special authorisation or for any other request.

If you've successfuly registered an account and logged in, you'll see your username by clicking on the menu at top right corner:

![](../assets/username_menu.png)

## How to upload data?

First, please [register an account on Gitlab](#how-to-register-on-gitlab) if you haven't done so already.

When you're logged in as a user, you can create a "Project", which is a repository where you will be able to deposit files. To do so, click on [New project](https://git.digitaltransport4africa.org/projects/new) at the top of the page:

![](../assets/new_project_button.png)

1. Enter a project name, for example the name of the city if you want to upload a GTFS dataset for that location;
2. Select "Initialize repository with a README";
3. Click on "Create project" (leave the rest as is):

![](../assets/create_project_form.png)

Congratulations, you have created your first repository! It looks something like this:

![](../assets/new_project_success.png)

You can now start **uploading** files in this repository, to do so click on the + menu then "Upload file":

![](../assets/upload_button.png)

An "Upload New File" menu will appear
1. "Click to upload" then find the file you want to upload from your computer;
2. Write a comment to describe your action (also called a "commit");
3. Click on "Upload file":

![](../assets/upload_menu.png)

Congratulations, you have uploaded your first bit of data!

![](../assets/upload_success.png)

You can share this link with everyone on the Internet now.

## How to add a license?

_Coming soon_

## How to create a new file?

On Gitlab you can create a file directly within the interface, for example to write new documentation. To do so, go to your repository where you want to create a new file, click on the + menu then "New file":

![](../assets/new_file_button.png)

1. Add your text;
2. Add a filename (for example `README.md`);
3. Write a comment to describe your action (also called a "commit");
4. When you're finished, click on "Commit changes" at the bottom:

![](../assets/new_file_page.png)

## How to edit a document?

On Gitlab you edit text files directly within the interface, for example to edit existing documentation. To do so, go to your repository where you want to edit a file and click on the file:

![](../assets/file.png)

Then click on the "Edit" button:

![](../assets/file_edit_button.png)

Modify the text, write a "commit message" to describe your action and when you're finished, click on "Commit changes" at the bottom.

## How to document my data? What's Markdown?

On Gitlab, each repository can be documented by creating a text file called a "README". It will be displayed by default when you access the repository's page, so it's pretty useful to have one!

It's best to format the README using Markdown. A Markdown document is just a basic text file ending with `.md`, and using some simple formating. If you're not familiar with it, please take a look at [this guide](https://daringfireball.net/projects/markdown/syntax), or read [the Wikipedia article about it](https://en.wikipedia.org/wiki/Markdown).

To document your data, create a file named `README.md` or edit it if you already have one in your repository. (See How to create a new file? and How to edit a document?)

## What's a "project"? What are "groups"?

In Gitlab, a "project" is a repository (or "repo") where users can deposit files; a bit like a folder on your computer. Each project is managed by one or several users who have rights on it. A "group" is just a folder to bundle several repositories together.

New users with basic access can only create projects in their own username group, whereas administrators can create new public groups and do many other things to organize the Gitlab platform.

## How is Gitlab organized?

As part of DT4A, Gitlab is jointly administered by [AFD](https://afd.fr/) and [WRI](https://wri.org), who make decisions on the way it is organized and managed.

Once [you've registered](#how-to-register-on-gitlab), your account on Gitlab has the lowest level of permissions. Further access & permissions can be given to you by administrators according to your status.

Gitlab repositories are categorized in several [public groups](https://git.digitaltransport4africa.org/explore/groups):

![](../assets/gitlab_groups.png)

If you want too add a repository you've created to one public group, please contact an administrator.
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTkxNDI0NzIyOF19
-->
