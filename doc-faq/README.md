# Documentation FAQ

Here are some of the most Frequently Asked Questions about documentation for [Digital Transport for Africa](http://digitaltransport4africa.org/). Feel free to [ask new questions](https://git.digitaltransport4africa.org/doc/gitlab-doc/issues/new)!

## What's a documentation?

Documentation is just a way to help other people understand something that you did, and learn from it. To do that, we propose that you use [Gitbook](https://www.gitbook.com/).

## What's Gitbook?

Gitbook is a popular tool that "makes it easy to write and maintain high-quality documentation". Technically, it's a piece of software which is both [open source](https://github.com/GitbookIO/gitbook) (meaning that you can install it and use it on your own computer or server), and is also available as [a service on Gitbook.com](https://www.gitbook.com/pricing).

Basically, **Gitbook takes formated text and transform it into a small website that you can browse like a book** (hence the name).

This way you only have to focus on the essential: the content. Gitbook takes care of the rest for you: formating, making a nice interactive website and all that.

Digital Transport for Africa maintains an instance of [Gitlab](https://about.gitlab.com/) which allows you to host, publish and share all sorts of resources: code, data, text and [static websites](https://en.wikipedia.org/wiki/Static_web_page), which means - you guessed it - that we can host Gitbooks (thanks to [Gitlab Pages](https://about.gitlab.com/product/pages/) and [Gitlab CI](https://about.gitlab.com/product/continuous-integration/)).

## How does this work? Can you show me an example of a Gitbook hosted on Digital Transport for Africa?

[You can find the basic template here](https://git.digitaltransport4africa.org/doc/gitbook). What you see is a repository with files in it, the "git" part of the name. It contains mainly the raw content of the documentation: a `README` and a `SUMMARY`.

[Gitlab then deploys this as a small website](http://doc.digitaltransport.io/gitbook): the "book"! The content of the `README` forms the content of the page, while the content of `SUMMARY` makes the left column a table of contents.

Another, more complex example: [git repo](https://git.digitaltransport4africa.org/doc/data-transport-mali-GTFS/) -> [book](http://doc.digitaltransport.io/data-transport-mali-GTFS). [Here are other real-world templates to model from](https://github.com/GitbookIO/gitbook/blob/master/docs/examples.md).

Note: When you change the content of the git repository, the book is built automatically You don't need to do anything but editing content. Also, the book's domain name (digitaltransport.io) has to be different than Gitlab's (digitaltransport4africa.org) for security reasons (cookies and stuff).

## How do I need to format my text?

Just use Markdown: a basic text file ending with `.md`, and some simple formating. If you're not familiar with it, please take a look at [this guide](https://daringfireball.net/projects/markdown/syntax), or read [the Wikipedia article about it](https://en.wikipedia.org/wiki/Markdown).

## How can I create my own Gitbook?

You don't have to understand the technical details. Here's all that you need to know:

1. Choose a template that you like, such as [the one above](https://git.digitaltransport4africa.org/doc/gitbook).
2. Fork it! Click on the button at the upper right corner. Then, select your space (where the git repo will be copied).
3. Edit the content of the `README.md` (which, by convention, will be the first page of your gitbook) by clicking on it in the list of files, then clicking on "Edit" in the upper right corner.
4. When you're finished, click on "Commit changes" at the bottom.
5. That's it! Your first page is done, and it will now be deployed as your own gitbook. Go find it by opening this URL in your browser: `<your namespace>.digitaltransport.io/gitbook` (with the namespace being most likely your Gitlab username, the same that appears in the URL of your repository).

Things you can do next:
6. Add other pages to your book by creating other Markdown files: click on "New file"

![](../assets/new_file_button.png)

Then, add your text (just as you did at step 3 above) and don't forget to type a filename ending with `.md`. Rince and repeat for each new page of your book.
7. Don't forget to also update the `SUMMARY.md` file by adding each new page.

Note: You can also clone the repo locally and edit the files on your computer with your editor of choice, then pushing each changes to the repo.